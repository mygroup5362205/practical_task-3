package com.kazmiruk.file;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.kazmiruk.message.product.Product;
import com.kazmiruk.message.validation.Error;
import com.kazmiruk.message.validation.ErrorResponse;
import com.opencsv.CSVReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CsvProductWriterTest {


    private CsvProductWriter writer;

    @BeforeEach
    public void setUp() throws IOException {
        writer = new CsvProductWriter();
    }


    @Test
    void shouldValidateMessagesAndWriteToCSVFile() throws Exception {
        List<Product> productList = List.of(
                new Product("dvfvfha", 5, LocalDateTime.now()),
                new Product("cfkkkofgba", 13, LocalDateTime.now()),
                new Product("ririvmvia", 14, LocalDateTime.now())
        );
        writer.validateMessagesAndWriteToCSVFile(productList);
        writer.close();
        assertAll(
                () -> assertNotNull(Thread.currentThread().getContextClassLoader().getResource(CsvProductWriter.FILE_NAME_INVALID_VALUES),
                        String.format("File %s does not exist", CsvProductWriter.FILE_NAME_INVALID_VALUES)),
                () -> assertNotNull(Thread.currentThread().getContextClassLoader().getResource(CsvProductWriter.FILE_NAME_VALID_VALUES),
                        String.format("File %s does not exist", CsvProductWriter.FILE_NAME_VALID_VALUES))
        );

        Path pathValid = Paths.get(
                ClassLoader.getSystemResource(CsvProductWriter.FILE_NAME_VALID_VALUES).toURI()
        );
        List<String[]> actualListValid = readAllLines(pathValid);

        List<String[]> expectedListValid = List.of(
                new String[]{"Name", "Count"},
                new String[]{"cfkkkofgba", "13"},
                new String[]{"ririvmvia", "14"}
        );
        assertAll(
                () -> assertArrayEquals(expectedListValid.get(0), actualListValid.get(0), "Header is incorrect"),
                () -> assertArrayEquals(expectedListValid.get(1), actualListValid.get(1), "2 row is incorrect"),
                () -> assertEquals(expectedListValid.get(2), expectedListValid.get(2), "3 row is incorrect")
        );

        Path pathInvalid = Paths.get(
                ClassLoader.getSystemResource(CsvProductWriter.FILE_NAME_INVALID_VALUES).toURI()
        );

        List<String[]> actualListInvalid = readAllLines(pathInvalid);

        for (int i = 0; i < actualListInvalid.get(1).length; i++) {
            actualListInvalid.get(1)[i] = actualListInvalid.get(1)[i].replaceAll("\"\"", "");
        }

        ObjectMapper objectMapper = new JsonMapper();
        List<String[]> expectedListInvalid = List.of(
                new String[]{"Name", "Count", "Errors"},
                new String[]{"dvfvfha", "5",  objectMapper.writeValueAsString(
                        new ErrorResponse(List.of(
                                new Error("The value of the count field must be >= 10"))
                        )
                )}
        );

        assertAll(
                () -> assertArrayEquals(expectedListInvalid.get(0), actualListInvalid.get(0), "Header is incorrect"),
                () -> assertArrayEquals(expectedListInvalid.get(1), actualListInvalid.get(1), "2 row is incorrect")
        );
    }

    public List<String[]> readAllLines(Path filePath) throws Exception {
        try (Reader reader = Files.newBufferedReader(filePath)) {
            try (CSVReader csvReader = new CSVReader(reader)) {
                return csvReader.readAll();
            }
        }
    }
}
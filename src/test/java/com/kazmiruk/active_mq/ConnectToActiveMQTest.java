package com.kazmiruk.active_mq;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.jms.Connection;

import static org.junit.jupiter.api.Assertions.*;

class ConnectToActiveMQTest {

    private ConnectToActiveMQ connectToActiveMQ;

    @BeforeEach
    void setUp() {
        connectToActiveMQ = ConnectToActiveMQ.getInstance();
    }

    @Test
    void shouldReturnConnection() {
        connectToActiveMQ.startConnection();
        if (!connectToActiveMQ.isConnectionOpen()) {
            connectToActiveMQ.startConnection();
        }
        Connection connection = connectToActiveMQ.getConnection();
        assertNotNull(connection);
    }

    @Test
    void shouldReturnSameInstance() {
        ConnectToActiveMQ connectToActiveMQ1 = ConnectToActiveMQ.getInstance();
        ConnectToActiveMQ connectToActiveMQ2 = ConnectToActiveMQ.getInstance();

        assertEquals(connectToActiveMQ1, connectToActiveMQ2);
    }

    @Test
    void shouldStopConnection() {
        connectToActiveMQ.stopConnection();
        Connection connection = connectToActiveMQ.getConnection();
        assertNull(connection,"Connection should be closed");
    }

    @Test
    void shouldReturnTrueWhenConnectionIsOpen() {
        connectToActiveMQ.startConnection();
        assertTrue(connectToActiveMQ.isConnectionOpen());
    }

    @Test
    void shouldReturnTrueWhenConnectionIsClose() {
        connectToActiveMQ.stopConnection();
        assertFalse(connectToActiveMQ.isConnectionOpen());
    }
}
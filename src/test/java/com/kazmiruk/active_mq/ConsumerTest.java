package com.kazmiruk.active_mq;

import com.kazmiruk.message.product.Product;
import com.kazmiruk.message.product.ProductJsonConverter;
import com.kazmiruk.threds.MessageSender;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.jms.*;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ConsumerTest {
    private static MessageConsumer messageConsumer;

    private static Consumer consumer;

    private static TextMessage textMessage;

    @BeforeAll
    static void setup() throws JMSException {
        Queue destination = mock(Queue.class);
        Session session = mock(Session.class);
        Connection connection = mock(Connection.class);
        messageConsumer = mock(MessageConsumer.class);
        textMessage = mock(TextMessage.class);

        when(connection.createSession(anyBoolean(), anyInt())).thenReturn(session);
        when(session.createQueue(ActiveMQConstants.QUEUE_NAME)).thenReturn(destination);
        when(session.createConsumer(destination)).thenReturn(messageConsumer);

        consumer = new Consumer(connection);
    }

    @Test
    void shouldReturnProductWhenTextMessageReceived() throws JMSException {
        Product expectedProduct = new Product("fgh", 10, LocalDateTime.now());

        ProductJsonConverter productJsonConverter = new ProductJsonConverter();

        when(messageConsumer.receive(1000)).thenReturn(textMessage);
        when(textMessage.getText()).thenReturn(productJsonConverter.fromObjectToJsonString(expectedProduct));

        Product actualProduct = consumer.receiveMessage();
        assertEquals(expectedProduct, actualProduct);
    }

    @Test
    void shouldReturnNullWhenPoisonPillMessageReceived() throws JMSException {
        when(messageConsumer.receive(1000)).thenReturn(textMessage);
        when(textMessage.getText()).thenReturn(MessageSender.POISON_PILL_MESSAGE);

        Product actualProduct = consumer.receiveMessage();

        assertNull(actualProduct);
    }

    @Test
    void shouldCloseSessionAndMessageConsumerWhenTheyAreNotNull() throws JMSException {
        assertTrue(consumer.isOpen());
        consumer.close();
        assertFalse(consumer.isOpen());
    }
}
package com.kazmiruk.message;

import com.kazmiruk.message.product.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {

    private Product product;

    @BeforeEach
    void setup() {
        product = new Product("abc", 4, LocalDateTime.of(2023, 3, 31, 1, 2));
    }

    @Test
    void shouldReturnCorrectValuesWhenGettersAreCalled() {
        assertAll(
                () -> assertEquals("abc", product.getName(), "Incorrect name"),
                () -> assertEquals(4, product.getCount(), "Incorrect count"),
                () -> assertEquals(LocalDateTime.of(2023, 3, 31, 1, 2),
                        product.getCreatedAt(), "Incorrect created at")
        );
    }

    @Test
    void shouldSetFieldValuesWhenSettersAreCalled() {
        String expectedName = "fgg";
        int expectedCount = 50;
        LocalDateTime expectedCreatedAt = LocalDateTime.of(2023, 3, 31, 1, 2);

        product.setName(expectedName);
        product.setCount(expectedCount);
        product.setCreatedAt(expectedCreatedAt);
        assertAll(
                () -> assertEquals(expectedName, product.getName(), "Incorrect name"),
                () -> assertEquals(expectedCount, product.getCount(), "Incorrect count"),
                () -> assertEquals(expectedCreatedAt, product.getCreatedAt(), "Incorrect created at")
        );
    }

    @Test
    void shouldReturnCorrectStringRepresentation() {
        String expectedProduct = String.format("%s[name='%s', count=%s, created_at=%s]",
                Product.class.getSimpleName(),
                product.getName(),
                product.getCount(),
                product.getCreatedAt()
        );
        assertEquals(expectedProduct, product.toString());
    }

    @Test
    void shouldGenerateRandomFieldValuesForProductObject() {
        Product productThatEqual = new Product("abc", 4, LocalDateTime.of(2023, 3, 31, 1, 2));
        Product productThatNotEqual = new Product("fgv", 7, LocalDateTime.of(2021, 3, 5, 6, 1, 3));
        assertAll(
                () -> assertEquals(product, productThatEqual),
                () -> assertNotEquals(productThatEqual, productThatNotEqual)
        );
    }

}
package com.kazmiruk.message.product;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class ProductJsonConverterTest {

    private final ProductJsonConverter productJsonConverter = new ProductJsonConverter();

    @Test
    void shouldMakeJsonStringFromProductObject() {
        LocalDateTime dateTime = LocalDateTime.of(2023, 4, 3, 1, 4, 4);
        Product product = new Product("abc", 10, dateTime);
        String expectedJSON = String.format("{\"name\":\"%s\",\"count\":%d,\"createdAt\":\"%s\"}",
                product.getName(), product.getCount(), product.getCreatedAt());
        String actualJSON = productJsonConverter.fromObjectToJsonString(product);
        assertEquals(expectedJSON, actualJSON);
    }

    @Test
    void shouldMakeProductObjectFromJsonString() {
        LocalDateTime dateTime = LocalDateTime.of(2023, 4, 3, 1, 4, 4);
        Product expectedProduct = new Product("abc", 10, dateTime);
        String productJson = String.format("{\"name\":\"%s\",\"count\":%d,\"createdAt\":\"%s\"}",
                expectedProduct.getName(), expectedProduct.getCount(), expectedProduct.getCreatedAt());
        Product actualProduct = productJsonConverter.fromJsonStringToProductObject(productJson);
        assertEquals(expectedProduct, actualProduct);
    }

}
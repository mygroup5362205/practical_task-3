package com.kazmiruk.message;

import com.kazmiruk.message.product.Product;
import com.kazmiruk.message.product.ProductGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;

@ExtendWith(MockitoExtension.class)
class ProductGeneratorTest {
    @Mock
    private Random random;

    @Test
    void shouldGenerateRandomFieldValuesForProductObject() {

        Mockito.when(random.nextInt(anyInt()))
                .thenReturn(1)
                .thenReturn(6)
                .thenReturn(10)
                .thenReturn(3)
                .thenReturn(25)
                .thenReturn(7);
        Mockito.when(random.nextLong()).thenReturn(245676L);

        ProductGenerator rpg = new ProductGenerator(random);

        LocalDateTime expectedDateTime = LocalDateTime.of(2023, 4, 1, 0, 0, 0,0);
        expectedDateTime = expectedDateTime.plus(random.nextLong() % 1000 * 60 * 60 * 24 * 365, ChronoUnit.MILLIS);

        Product actualProduct = rpg.generate();
        Product expectedProduct = new Product("gkdz", 7, expectedDateTime);
        assertEquals(expectedProduct, actualProduct);
    }

}
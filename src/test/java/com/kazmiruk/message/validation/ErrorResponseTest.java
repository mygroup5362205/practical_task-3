package com.kazmiruk.message.validation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ErrorResponseTest {

    private List<Error> expectedErrors;

    @BeforeEach
    void setup() {
        Error error1 = new Error("Error1");
        Error error2 = new Error("Error2");
        Error error3 = new Error("Error3");

        expectedErrors = List.of(error1, error2, error3);
    }

    @Test
    void shouldReturnListOfErrorsWhenGetErrorsIsCalled() {
        ErrorResponse errorResponse = new ErrorResponse(expectedErrors);
        assertEquals(expectedErrors, errorResponse.getErrors());
    }

    @Test
    void shouldSetListOfErrorsWhenSetErrorsIsCalled() {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrors(expectedErrors);
        assertEquals(errorResponse.getErrors(), expectedErrors);
    }

}
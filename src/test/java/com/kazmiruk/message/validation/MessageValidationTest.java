package com.kazmiruk.message.validation;

import com.kazmiruk.message.product.Product;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MessageValidationTest {

    @Test
    void shouldReturnThreeErrorsWhenValidateProduct() {
        Product product = new Product("ffv", 5, LocalDateTime.of(2021, 4, 1, 7, 8, 1));

        MessageValidation messageValidation = new MessageValidation();
        List<Error> actualErrors = messageValidation.validate(product);
        List<Error> expectedErrors = new ArrayList<>(
                List.of(
                        new Error("The length of the \"name\" field must be => 7"),
                        new Error("The name must contain the letter 'a'"),
                        new Error("The value of the \"count\" field must be >= 10")
                )
        );
        expectedErrors.sort(Comparator.comparing(Error::getErrorMessage));
        actualErrors.sort(Comparator.comparing(Error::getErrorMessage));
        assertEquals(expectedErrors, actualErrors);
    }
}
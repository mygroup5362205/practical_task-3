package com.kazmiruk.threds;

import com.kazmiruk.active_mq.Producer;
import com.kazmiruk.message.product.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.JMSException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class MessageSender implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(MessageSender.class);

    public static final String POISON_PILL_MESSAGE = "THE LAST MESSAGE IN THE QUEUE";

    private final ArrayBlockingQueue<Product> products;

    private final AtomicBoolean allElementsGenerated;

    public MessageSender(ArrayBlockingQueue<Product> messages, AtomicBoolean allElementsGenerated) {
        this.products = messages;
        this.allElementsGenerated = allElementsGenerated;
    }
    @Override
    public void run() {
        Producer producer = new Producer();
        long startSend = System.currentTimeMillis();
        int messageCounter = 0;
        while (true) {
            try {
                Product product = products.take();
                producer.sendMessage(product);
                messageCounter++;
                if (products.isEmpty() && allElementsGenerated.get()) {
                    logger.info("All messages were sent");
                    producer.sendMessage(POISON_PILL_MESSAGE);
                    logger.info("Poison pill was sent");
                    producer.close();
                    long sentTime = System.currentTimeMillis() - startSend;
                    logger.info("SENDING SPEED: {} messages per second", messageCounter / (sentTime / 1000.0));
                    break;
                }
            } catch (InterruptedException | JMSException e) {
                logger.error(e.getMessage(), e);
                Thread.currentThread().interrupt();
            }
        }
    }
}

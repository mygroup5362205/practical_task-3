package com.kazmiruk.threds;

import com.kazmiruk.message.product.Product;
import com.kazmiruk.message.product.ProductGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

public class ProductsGenerator implements Runnable {

    private final Logger logger = LoggerFactory.getLogger(ProductsGenerator.class);

    private final ArrayBlockingQueue<Product> products;

    private final int maxCount;

    private final int maxTime;


    private final AtomicBoolean allElementsGenerated;

    public ProductsGenerator(ArrayBlockingQueue<Product> products, int maxCount, int maxTime, AtomicBoolean allElementsGenerated) {
        this.products = products;
        this.maxCount = maxCount;
        this.maxTime = maxTime * 1000;
        this.allElementsGenerated = allElementsGenerated;
    }

    @Override
    public void run() {
        ProductGenerator productGenerator = new ProductGenerator();
        long startTime = System.currentTimeMillis();
        Stream.generate(productGenerator::generate)
                .takeWhile(p -> System.currentTimeMillis() - startTime <= maxTime)
                .limit(maxCount)
                .forEach(product -> {
                    try {
                        products.put(product);
                        logger.info("Message was added to the blocking queue");
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        logger.error(e.getMessage(), e);
                    }
                });
        logger.info("All messages were added to the queue");
        allElementsGenerated.set(true);
    }
}

package com.kazmiruk.active_mq;

import com.kazmiruk.message.product.Product;
import com.kazmiruk.message.product.ProductJsonConverter;
import com.kazmiruk.threds.MessageSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import javax.jms.Message;
import java.util.LinkedList;
import java.util.List;

public class Consumer implements AutoCloseable, MessageListener  {

    private static final Logger logger = LoggerFactory.getLogger(Consumer.class);

    private Session session;

    private MessageConsumer messageConsumer;

    private List<Product> products;

    private long startReceive;

    public Consumer() {
        this(ConnectToActiveMQ.getInstance().getConnection());
    }

    public Consumer(Connection connection) {
        try {
            products = new LinkedList<>();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createQueue(ActiveMQConstants.QUEUE_NAME);
            messageConsumer = session.createConsumer(destination);
            messageConsumer.setMessageListener(this);
        } catch (JMSException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public Product receiveMessage(Message message) throws JMSException {
        Product product = null;
        ProductJsonConverter productJsonConverter = new ProductJsonConverter();

        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            if (MessageSender.POISON_PILL_MESSAGE.equals(textMessage.getText())) {
                return null;
            }
            product = productJsonConverter.fromJsonStringToProductObject(textMessage.getText());
            logger.info("Message was received");
        }
        return product;
    }

    public Product receiveMessage() throws JMSException {
        return receiveMessage(messageConsumer.receive(1000));
    }

    public boolean isOpen() {
        return session != null;
    }

    @Override
    public void close() throws JMSException {
        if (session != null) {
            session.close();
            session = null;
        }
        if (messageConsumer != null) {
            messageConsumer.close();
            messageConsumer = null;
        }
    }

    public List<Product> getProducts() {
        return products;
    }

    @Override
    public void onMessage(Message message) {
        try {
            if (!isOpen()) {
                return;
            }
            if (startReceive == 0) {
                startReceive = System.currentTimeMillis();
            }
            Product product = receiveMessage(message);
            if (product == null) {
                logger.info("Caught a poison pill");
                long receivedTime = System.currentTimeMillis() - startReceive;
                logger.info("RECEIVING SPEED: {} messages per second", products.size() / (receivedTime / 1000.0));
                close();
                return;
            }
            products.add(product);
        } catch (JMSException e) {
            logger.error(e.getMessage(), e);
        }
    }

}

package com.kazmiruk.active_mq;

import com.kazmiruk.message.product.Product;
import com.kazmiruk.message.product.ProductJsonConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;

public class Producer implements AutoCloseable {

    private static final Logger logger = LoggerFactory.getLogger(Producer.class);

    private MessageProducer messageProducer;

    private Session session;

    public Producer() {
        try {
            ConnectToActiveMQ connectToActiveMQ = ConnectToActiveMQ.getInstance();
            Connection connection = connectToActiveMQ.getConnection();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createQueue(ActiveMQConstants.QUEUE_NAME);
            messageProducer = session.createProducer(destination);
            messageProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            logger.info("Producer was created");
        } catch (JMSException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void sendMessage(Product product) throws JMSException {
        ProductJsonConverter productJsonConverter = new ProductJsonConverter();
        sendMessage(productJsonConverter.fromObjectToJsonString(product));
    }

    public void sendMessage(String message) throws JMSException {
        TextMessage textMessage = session.createTextMessage(message);
        messageProducer.send(textMessage);
        logger.info("Message sent successfully to the queue");
    }

    @Override
    public void close() throws JMSException {
        if (session != null) {
            session.close();
            logger.info("The session was closed");
            session = null;
        }
        if (messageProducer != null) {
            messageProducer.close();
            logger.info("The message producer was closed");
            messageProducer = null;
        }
    }
}

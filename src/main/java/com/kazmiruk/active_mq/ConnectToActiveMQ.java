package com.kazmiruk.active_mq;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.Connection;
import javax.jms.JMSException;
import java.util.LinkedList;
import java.util.List;

public final class ConnectToActiveMQ {
    private final Logger logger = LoggerFactory.getLogger(ConnectToActiveMQ.class);
    private static ConnectToActiveMQ instance;
    private final ActiveMQConnectionFactory connectionFactory;
    private Connection connection;

    private ConnectToActiveMQ() {
        connectionFactory =
                new ActiveMQConnectionFactory(ActiveMQConstants.WRITE_LEVEL_ENDPOINT);
        connectionFactory.setTrustedPackages(new LinkedList<>(List.of("com.kazmiruk.message.product")));
        connectionFactory.setUserName(ActiveMQConstants.USERNAME);
        connectionFactory.setPassword(ActiveMQConstants.PASSWORD);
        tryToCreateConnectionAndStart(connectionFactory);
    }

    private void tryToCreateConnectionAndStart(ActiveMQConnectionFactory connectionFactory) {
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            logger.info("A new connection has been created and started");
        } catch (JMSException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public static ConnectToActiveMQ getInstance() {
        if (instance == null) {
            instance = new ConnectToActiveMQ();
        }
        return instance;
    }

    public void startConnection() {
        if (connection == null) {
            tryToCreateConnectionAndStart(connectionFactory);
        }
    }

    public boolean isConnectionOpen() {
        return connection != null;
    }

    public void stopConnection() {
        try {
            if (connection != null) {
                connection.stop();
                connection.close();
                logger.info("Connection was closed");
            }
        } catch (JMSException e) {
            logger.error(e.getMessage(), e);
        } finally {
            connection = null;
        }
    }
}

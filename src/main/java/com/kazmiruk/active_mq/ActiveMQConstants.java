package com.kazmiruk.active_mq;

import com.kazmiruk.file.ExternalPropertiesFile;

public class ActiveMQConstants {
    private ActiveMQConstants() {}
    public static final String FILE_NAME = "connection.properties";

    public static final String WRITE_LEVEL_ENDPOINT;

    public static final String USERNAME;
    public static final String PASSWORD;

    public static final String QUEUE_NAME;

    public static final int GENERATING_TIME;

    static {
        ExternalPropertiesFile epf = new ExternalPropertiesFile(FILE_NAME);
        WRITE_LEVEL_ENDPOINT = epf.getProperty("open_wire_endpoint");
        USERNAME = epf.getProperty("username");
        PASSWORD = epf.getProperty("password");
        QUEUE_NAME = epf.getProperty("queue_name");
        GENERATING_TIME = Integer.parseInt(epf.getProperty("generating_time"));
    }
}

package com.kazmiruk.file;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.kazmiruk.message.validation.Error;
import com.kazmiruk.message.product.Product;
import com.kazmiruk.message.validation.ErrorResponse;
import com.kazmiruk.message.validation.MessageValidation;
import com.opencsv.CSVWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class CsvProductWriter implements AutoCloseable {

    private static final Logger logger = LoggerFactory.getLogger(CsvProductWriter.class);

    public static final String FILE_NAME_VALID_VALUES = "valid.csv";

    public static final String FILE_NAME_INVALID_VALUES = "invalid.csv";

    private FileWriter outputFileForValid;

    private FileWriter outputFileForInvalid;

    private CSVWriter writerForValid;

    private CSVWriter writerForInvalid;


    public CsvProductWriter() throws IOException {
        String path = Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource("")).getPath();
        File fileForValid = new File(path + FILE_NAME_VALID_VALUES);
        File fileForInvalid = new File(path + FILE_NAME_INVALID_VALUES);
        if (!fileForValid.exists()) {
            fileForValid.createNewFile();
        }
        if (!fileForInvalid.exists()) {
            fileForInvalid.createNewFile();
        }

        outputFileForValid = new FileWriter(fileForValid);
        outputFileForInvalid = new FileWriter(fileForInvalid);

        writerForValid = new CSVWriter(outputFileForValid);
        writerForInvalid = new CSVWriter(outputFileForInvalid);

        writerForValid.writeNext(new String[]{"Name", "Count"});
        writerForInvalid.writeNext(new String[]{"Name", "Count", "Errors"});
    }

    public void validateMessagesAndWriteToCSVFile(List<Product> products) throws JsonProcessingException {
        for (Product product : products) {
            validateMessageAndWriteToCSVFile(product);
        }
    }

    public void validateMessageAndWriteToCSVFile(Product product) throws JsonProcessingException {
        MessageValidation messageValidation = new MessageValidation();
        List<Error> errors = messageValidation.validate(product);
        if (errors.isEmpty()) {
            writeValidMessageToFile(product);
        } else {
            writeInvalidMessageToFile(product, errors);
        }
    }


    private void writeValidMessageToFile(Product product) {
        writerForValid.writeNext(new String[]{product.getName(), String.valueOf(product.getCount())});
        logger.info("{} was written to the file {}", product, FILE_NAME_VALID_VALUES);
    }

    private void writeInvalidMessageToFile(Product product, List<Error> errors) throws JsonProcessingException {
        ObjectMapper objectMapper = new JsonMapper();
        ErrorResponse errorResponse = new ErrorResponse(errors);

        writerForInvalid.writeNext(new String[] {
                product.getName(),
                String.valueOf(product.getCount()),
                objectMapper.writeValueAsString(errorResponse)
        });

        logger.info("{} and errors was written to the file {}", product, FILE_NAME_INVALID_VALUES);
    }

    @Override
    public void close() throws IOException {
        if (writerForInvalid != null) {
            writerForInvalid.close();
            writerForInvalid = null;
        }
        if (writerForValid != null) {
            writerForValid.close();
            writerForValid = null;
        }
        if (outputFileForInvalid != null) {
            outputFileForInvalid.close();
            outputFileForInvalid = null;
        }
        if (outputFileForValid != null) {
            outputFileForValid.close();
            outputFileForValid = null;
        }
    }
}

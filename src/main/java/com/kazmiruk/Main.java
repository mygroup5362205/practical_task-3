package com.kazmiruk;

import com.kazmiruk.active_mq.*;
import com.kazmiruk.file.CsvProductWriter;
import com.kazmiruk.message.product.Product;
import com.kazmiruk.threds.MessageSender;
import com.kazmiruk.threds.ProductsGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        try {
            int numOfMessages = 1000;
            if (args.length != 0) {
                numOfMessages = Integer.parseInt(args[0]);
            }

            logger.info("{} messages will be generated in {} milliseconds", numOfMessages, ActiveMQConstants.GENERATING_TIME);

            final int maxCapacityOfQueue = 100;

            ArrayBlockingQueue<Product> products = new ArrayBlockingQueue<>(maxCapacityOfQueue);
            AtomicBoolean allElementsGenerated = new AtomicBoolean(false);
            ExecutorService executor = Executors.newFixedThreadPool(3);

            Consumer consumer = new Consumer();

            executor.execute(new ProductsGenerator(products, numOfMessages, ActiveMQConstants.GENERATING_TIME, allElementsGenerated));
            executor.execute(new MessageSender(products, allElementsGenerated));

            executor.shutdown();

            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

            Thread.sleep(5000);

            List<Product> receiverProducts = consumer.getProducts();

            CsvProductWriter csvProductWriter = new CsvProductWriter();
            csvProductWriter.validateMessagesAndWriteToCSVFile(receiverProducts);

            consumer.close();
            csvProductWriter.close();
            ConnectToActiveMQ.getInstance().stopConnection();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            Thread.currentThread().interrupt();
        }
    }
}
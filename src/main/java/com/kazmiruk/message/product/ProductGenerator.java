package com.kazmiruk.message.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Random;

public class ProductGenerator {

    private static final Logger logger = LoggerFactory.getLogger(ProductGenerator.class);

    private final Random random;

    private static final int MAX_N = 20;
    private static final int MIN_WORD_LENGTH = 3;
    private static final int MAX_WORD_LENGTH = 15;

    public ProductGenerator() {
        random = new Random();
    }

    public ProductGenerator(Random random) {
        this.random = random;
    }

    public Product generate() {
        Product product = new Product(
                generateRandomString(),
                random.nextInt(MAX_N),
                generateRandomLocalDateTime());
        logger.info("{} was randomly generated", product);
        return product;
    }

    private String generateRandomString() {
        int wordLength = MIN_WORD_LENGTH + random.nextInt(MAX_WORD_LENGTH + 1 - MIN_WORD_LENGTH);
        StringBuilder word = new StringBuilder();
        final int numOfLetters = 26;

        for (int i = 0; i < wordLength; i++) {
            word.append((char) (random.nextInt(numOfLetters) + 'a'));
        }
        return word.toString();
    }


    private LocalDateTime generateRandomLocalDateTime() {
        final int millisecondsInSecond = 1000;
        final int secondsInMinute = 60;
        final int minutesInHour = 60;
        final int hoursInDay = 24;
        final int daysInYear = 365;

        long randomMillisecondsToAddOrSubtract = random.nextLong() %
                millisecondsInSecond * secondsInMinute * minutesInHour * hoursInDay * daysInYear;
        LocalDateTime pointDate = LocalDateTime.of(2023, 4, 1, 0, 0, 0,0);
        return pointDate.plus(randomMillisecondsToAddOrSubtract, ChronoUnit.MILLIS);
    }
}

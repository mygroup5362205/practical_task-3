package com.kazmiruk.message.product;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProductJsonConverter {

    private final Logger logger = LoggerFactory.getLogger(ProductJsonConverter.class);

    private final ObjectMapper objectMapper = new JsonMapper();

    public ProductJsonConverter() {
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    public Product fromJsonStringToProductObject(String product) {
        try {
            return objectMapper.readValue(product, Product.class);
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public String fromObjectToJsonString(Product product) {
        String productJSON = null;
        try {
            productJSON = objectMapper.writeValueAsString(product);
            logger.info("Object \"{}\" converted to \"{}\"", product, productJSON);
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage(), e);
        }
        return productJSON;
    }


}

package com.kazmiruk.message.product;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Product implements Serializable {
    @Size(min = 7, message = "The length of the \"name\" field must be => 7")
    @Pattern(regexp = ".*a.*", message = "The name must contain the letter 'a'")
    private String name;


    @Min(value = 10, message = "The value of the \"count\" field must be >= 10")
    private int count;
    private LocalDateTime createdAt;

    public Product() {}

    public Product(String name, int count, LocalDateTime createdAt) {
        this.name = name;
        this.count = count;
        this.createdAt = createdAt;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }


    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    @Override
    public String toString() {
        return String.format("%s[name='%s', count=%s, created_at=%s]",
                this.getClass().getSimpleName(), name, count, createdAt);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Product)) {
            return false;
        }

        Product other = (Product) obj;

        return this.count == other.count
                && this.name.equals(other.name)
                && this.createdAt.equals(other.createdAt);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + count;
        result = 31 * result + name.hashCode();
        result = 31 * result + createdAt.hashCode();
        return result;
    }
}

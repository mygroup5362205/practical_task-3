package com.kazmiruk.message.validation;

import com.kazmiruk.message.product.Product;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.hibernate.validator.messageinterpolation.ParameterMessageInterpolator;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class MessageValidation {

    private final Validator validator;

    public MessageValidation() {
        ValidatorFactory factory = Validation.byDefaultProvider()
                .configure()
                .messageInterpolator(new ParameterMessageInterpolator())
                .buildValidatorFactory();
        validator = factory.getValidator();
    }

    public List<Error> validate(Product product) {
        Set<ConstraintViolation<Product>> violations = validator.validate(product);
        return violations.stream().map(v -> new Error(v.getMessage())).collect(Collectors.toList());
    }

}
